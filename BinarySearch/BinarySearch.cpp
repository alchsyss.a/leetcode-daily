#include <bits/stdc++.h>
using namespace std;

#define ll long long
#define lli long long int
#define F              first
#define S              second
#define pb             push_back
#define double         long double
#define que_max        priority_queue <int>
#define que_min        priority_queue <int, vi, greater<int>>
#define print(a)       for(auto x : a) cout << x << " "; cout << endl
#define mod 1000000007

inline int power(int a, int b){
    int x = 1;
    while (b){
        if (b & 1) x *= a;
        a *= a; b >>= 1;
    }
    return x;
}

class Solution {
    private:
    public:

	/*
	Binary Search, TC O(logN)
	a) In case of monotonic functions/ sorted
	b) In the case(s) when search space for ans can be restricted.

	INT ~ [2^31, 2^31-1]
	*/
	int binarySearch(vector<int> arr, int target){
		int start = 0, end = arr.size()-1;
		int mid = start + (end-start)/2;

		while(start <= end){
			if(arr[mid] == target){ return mid; }
			else if(arr[mid] > target){ end = mid-1; }
			else { start = mid+1; }

			mid = start + (end-start)/2;
		}
		return -1;
	}

    // First occurence,
    // idea is keep of searching on the left even if the element is found while (start<=end)
    int firstOccurenceHelper(vector<int> &arr, int n, int target){
		int start = 0, end = arr.size()-1;
		int mid = start + (end-start)/2;
		int ans = -1;

		while(start <= end){
			if(arr[mid] == target){ ans = mid; end = mid-1;  }
			else if(arr[mid] > target){ end = mid-1; }
			else { start = mid+1; }

			mid = start + (end-start)/2;
		}
		return ans;    	
    }

    int lastOccurenceHelper(vector<int> &arr, int n, int target){
		int start = 0, end = arr.size()-1;
		int mid = start + (end-start)/2;
		int ans = -1;

		while(start <= end){
			if(arr[mid] == target){ ans = mid; start = mid+1;  }
			else if(arr[mid] > target){ end = mid-1; }
			else { start = mid+1; }

			mid = start + (end-start)/2;
		}
		return ans;    	
    }

    /*
    pair<int, int> firstAndLastPosition(vector<int> &arr,  int key){
   		pair<int, int> p; int n = arr.size();
   		p = make_pair((firstOccurenceHelper(arr, n, key), lastOccurenceHelper(arr, n, key));

   		return p;
    }
	*/
	
    // peak index in mountain array
    int returnPeak(vector<int> &arr){
		int start = 0, end = arr.size()-1;
		int mid = start + (end-start)/2;

		while(start < end){
			if(arr[mid] < arr[mid+1]){ start = mid+1; }
			else { end = mid; }

			mid = start + (end-start)/2;
		}
		return start;
    }

    // find pivot in a rotated sorted array
    int findPivot(vector<int> &arr, int n){
		int start = 0, end = arr.size()-1;
		int mid = start + (end-start)/2;

		while(start < end){
			if(arr[mid] > arr[0]){ start = mid+1; }
			else { end = mid; }

			mid = start + (end-start)/2;
		}
		return start;
    }

    // find sqrt using binary search
	double morePrecision(int n, int precision, int tempSol){
		double factor = 1;
		double ans = tempSol;

		for(int i=0;i<precision;i++){
			factor = factor/10;

			for(double j=ans;j*j<n;j+=factor){ ans = j; }
		}
		return ans;
	}
    
	int findSqrtHelper(int number){
		int start = 1, end = number;
		int mid = start + (end-start)/2;

		while(start <= end){
			long long int square = mid*mid;

			if(square == n){ return mid; }
			if(square < n){
				ans = mid;
				start = mid+1;
			} else { end = mid-1; }
			mid = start + (end-start)/2;
		}
		return ans;
	}
    
    int findSqrt(int number){
    	return findSqrtHelper(number);
    }
};

void solve(){
    return;
}

int32_t main() {
    ios_base::sync_with_stdio(false);cin.tie(NULL);cout.tie(NULL);
//#ifndef ONLINE_JUDGE
//	freopen("input.txt", "r", stdin);
//	freopen("output.txt", "w", stdout);
//#endif

    ll test=1;
    //cin>>test;
    while(test--){
        // solve();
        Solution sol;
        vector<int> arr = {2,7,8,6,2,2,1};
        vector<int> arr2 = {8,10,17,1,3};
        cout<<sol.returnPeak(arr)<<endl;
        cout<<sol.findPivot(arr2, 5)<<endl;
    }

    return 0;
}
