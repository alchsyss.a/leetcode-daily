#include <bits/stdc++.h>
using namespace std;

#define ll long long
#define lli long long int
#define F              first
#define S              second
#define pb             push_back
#define double         long double
#define que_max        priority_queue <int>
#define que_min        priority_queue <int, vi, greater<int>>
#define print(a)       for(auto x : a) cout << x << " "; cout << endl
#define mod 1000000007

inline int power(int a, int b){
    int x = 1;
    while (b){
        if (b & 1) x *= a;
        a *= a; b >>= 1;
    }
    return x;
}

class Solution {
    private:
    public:

	/*
	Permutations,
	for a string of length 'n' we can have n! possible permutations
	*/
	// Permutations approach-1
	void permuteHelper(vector<int> &ds, vector<int> &nums, vector<vector<int>> &ans, int freqArray[]){
		// base case, when we finish reading our input
		if(ds.size() == nums.size()){
			ans.push_back(ds);
			return;
		}

		// call for every element in nums
		for(int i=0;i<nums.size();i++){
			if(!freqArray[i]){ // if current ele is not a part of permutation
				ds.push_back(nums[i]);
				freqArray[i] = 1;
				permuteHelper(ds, nums, ans, freqArray);
				freqArray[i] = 0; // while returning from the function call
				ds.pop_back();
			}
		}
	}
	
	vector<vector<int>> permute(vector<int> &nums){
		vector<vector<int>> ans;
		vector<int> ds;
		int freqArray[nums.size()];

		for(int i=0;i<nums.size();i++){ freqArray[i] = 0; }
		permuteHelper(ds, nums, ans, freqArray);
		return ans;
	}

	// Permutations approach-22
	void permuteHelper2nd(int index, vector<int> &nums, vector<vector<int>> &ans){
		if(index == nums.size()){
			ans.push_back(nums);
			return;
		}

		for(int i=index;i<nums.size();i++){
			swap(nums[index], nums[i]);
			permuteHelper2nd(index+1, nums, ans);
			swap(nums[index], nums[i]);
		}
	}
	
	vector<vector<int>> permute2nd(vector<int> &nums){
		vector<vector<int>> ans;
		permuteHelper2nd(0, nums, ans);
		return ans;
	}

	// Combination Sum
	
    
};

void solve(){
    return;
}

int32_t main() {
    ios_base::sync_with_stdio(false);cin.tie(NULL);cout.tie(NULL);
//#ifndef ONLINE_JUDGE
//	freopen("input.txt", "r", stdin);
//	freopen("output.txt", "w", stdout);
//#endif

    ll test=1;
    //cin>>test;
    while(test--){
        // solve();
        Solution sol;
        vector<int> nums = {1,2,3,4};
        vector<vector<int>> ans = sol.permute(nums);
    }

    return 0;
}
