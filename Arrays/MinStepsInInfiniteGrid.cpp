#include <bits/stdc++.h>
using namespace std;

#define ll long long
#define lli long long int
#define F              first
#define S              second
#define pb             push_back
#define double         long double
#define que_max        priority_queue <int>
#define que_min        priority_queue <int, vi, greater<int>>
#define print(a)       for(auto x : a) cout << x << " "; cout << endl
#define mod 1000000007

inline int power(int a, int b){
    int x = 1;
    while (b){
        if (b & 1) x *= a;
        a *= a; b >>= 1;
    }
    return x;
}

class Solution {
    private:
    public:

    int coverPoints(vector<int> &arr1, vector<int> &arr2) {
	    int steps = 0;
	    for(int i=0;i<arr1.size()-1;i++){
	        steps += max(abs(arr1[i+1]-arr1[i]), abs(arr2[i+1]-arr2[i]));
	    }
	    return steps;
    }

	bool allNines(vector<int> &vec){
		for(int x : vec){
			if(x != 9) return false;
		}
		return true;
	}

	vector<int> plusOne(vector<int> &nums){
		int n = nums.size()-1; vector<int> ans;

		int i = n;
		if(nums[i] != 9){ nums[i] += 1; return nums; }

		if(allNines(nums)){
			ans.push_back(1);
			ans.push_back(0);
			while(n != 0){
				ans.push_back(0);
				n--;
			}
		} else {
			int carry = 1;
			nums[i] = 0;
			i--;
			while(1){
				if(nums[i] != 9){ nums[i] += carry; return nums; }
				nums[i] = 0;
				i--;
			}
		}
		return ans;
	}

	vector<int> flip(string s){
		int n = s.size();
		int left = 1, right = 1, leftAns, rightAns, diff = 0;
		vector<int> ans; bool flag = false;
		for(char ch : s){
			if(ch != '1') flag = true;
		}
		if(flag == false) return ans;
		
		unordered_map<int, int> mpp;
		
		// 1 -> diff-- and 0 ->diff++
		for(int i=0;i<n;i++){
			if(s[i] == '0'){
				diff += 1;
				
				if(diff == 1){
					left = i+1;
					if(mpp[diff] == 0){
						right = i+1;
						mpp[diff] = right;	
					}
				} else {
					if(mpp[diff] == 0){
						right = i+1;
						mpp[diff] = right;	
					}
				}
			} else {
				diff -= 1;
				if(diff < 0) diff = 0;
			}
		}	

		if(right < left){ left = right; }
		ans.push_back(left); ans.push_back(right);
		return ans;
	}
    
};

void solve(){
    return;
}

int32_t main() {
    ios_base::sync_with_stdio(false);cin.tie(NULL);cout.tie(NULL);
//#ifndef ONLINE_JUDGE
//	freopen("input.txt", "r", stdin);
//	freopen("output.txt", "w", stdout);
//#endif

    ll test=1;
    //cin>>test;
    while(test--){
        // solve();
        Solution sol;
        vector<int> arr1 = {0,1,1};
        vector<int> arr2 = {0,1,2};

        cout<<sol.coverPoints(arr1, arr2)<<endl;
	
    	vector<int> v1 = {4,3,2,1};    
    	vector<int> v2 = {1,2,9,9};    
    	vector<int> v3 = {9,9,9,9};    

    	vector<int> a1; vector<int> a2; vector<int> a3;

		a1 = sol.plusOne(v1);
		a2 = sol.plusOne(v2);
		a3 = sol.plusOne(v3);

    	for(int x : a1){
    		cout<<x<<" ";
    	} cout<<endl;

    	for(int x : a2){
    		cout<<x<<" ";
    	} cout<<endl;

  		for(int x : a3){
    		cout<<x<<" ";
    	} cout<<endl;

		string s = "111";
		// string s = "1011000101";
		// string s = "101001";
		// string s = "0010";
		// string s = "010";
    	vector<int> flipRet = sol.flip(s);

    	for(int x : flipRet){
    		cout<<x<<" ";
    	} cout<<endl;
    }

    return 0;
}
